package com.qf.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import javax.servlet.http.HttpSession;
import java.util.Random;

public class SmsUtil {
    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    static final String accessKeyId = "LTAI4G6zBwfYENfm1Q8jGGQT";
    static final String accessKeySecret = "Jycedw8EJnMgwOAJf4yje0m7UL5Unh";

    public static SendSmsResponse sendSms(String phone,String code) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("玩吧游戏商城个人学习使用");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_199201086");
        //模板中code参数,接受按照json字符串进行接受
        request.setTemplateParam("{'code':"+code+"}");
        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsRequest=null;
        try{
            sendSmsRequest=acsClient.getAcsResponse(request);
        }catch (Exception e){
            e.printStackTrace();
        }
        return sendSmsRequest;

    }

    public static String createCode(){
        String code=(int)((Math.random()*9+1)*100000)+"";
        System.out.println("创建的验证码为："+code);
        return code;
    }



}

