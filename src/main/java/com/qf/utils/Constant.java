package com.qf.utils;

/**
 * 存放常量
 */
public class Constant {

    //用来做hash运算的移位值
    public static final Integer HASH_CARRY_DIGIT = (1 << 4) - 1;
    //下划线常量
    public static final String UNDERLINE_SEPARATOR = "_";
    //web服务器中的请求分割符
    public static final String REQUEST_SEPARATOR = "/";



}
