package com.qf.controller;

import com.qf.entity.DTO.MessageDTO;
import com.qf.entity.Message;
import com.qf.entity.User;
import com.qf.service.IMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * message页面请求信息
 */
@RestController
@RequestMapping("/message")
public class MsgController {
    @Autowired
    private IMsgService msgService;

    @RequestMapping("/info/{uId}")

    public MessageDTO findMsgInfo(@PathVariable("uId") Integer uId){
        List<User> userList = msgService.findNearPeople(uId);
        List<Message> msgList = msgService.findMsgList(uId);
        MessageDTO messageDTO = new MessageDTO(userList,msgList);
        return messageDTO;
    }

}
