package com.qf.controller;

import com.qf.entity.DTO.RoomDTO;
import com.qf.entity.Party;
import com.qf.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    private RoomService roomService;

    @RequestMapping("/select")
    public List getRoomList(RoomDTO roomDTO){
        return roomService.selectAll(roomDTO);
    }

    @RequestMapping("/party")
    public List<Party> getPartyList(Party party){
        return roomService.selectParty(party);
    }
}
