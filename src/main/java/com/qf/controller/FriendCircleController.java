package com.qf.controller;

import com.alibaba.fastjson.JSON;
import com.qf.entity.DTO.FriendCircleDTO;
import com.qf.service.IFriendCircleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/friendCircle")
public class FriendCircleController {

    @Autowired
    private IFriendCircleService circleService;

    @RequestMapping("/info/{uId}")
    public String getFriendCircle(@PathVariable("uId") Integer uId){
        //获取结果数据
        List<FriendCircleDTO> list = circleService.getFriendCircleDTOList(uId);
        //转String类型,并返回
        return JSON.toJSONString(list);
    }





}

