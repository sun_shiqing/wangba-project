package com.qf.listener;

import org.springframework.stereotype.Component;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * @description:
 * @author: ssq
 * @time: 2020/8/5 20:57
 */
@Component
public class Mylistener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.println("destroy方法执行了");
    }


    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println("initialize方法执行了");
    }
}

