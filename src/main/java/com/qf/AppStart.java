package com.qf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //表示springBoot启动应用程序 注解
@MapperScan("com.qf.dao")//接口扫描
public class AppStart {
    public static void main(String[] args) {
        //启动应用程序
        SpringApplication.run(AppStart.class,args);
    }
}
