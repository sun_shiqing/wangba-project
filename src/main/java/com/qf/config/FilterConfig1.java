package com.qf.config;

import com.qf.filter.EncodingFilter;
import com.qf.filter.MyFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description:
 * @author: ssq
 * @time: 2020/8/5 20:51
 */
@Configuration
public class FilterConfig1 {

    @Autowired
    private MyFilter myFilter;
    @Autowired
    private EncodingFilter encodingFilter;

    @Bean
    public FilterRegistrationBean getFilter(){

        FilterRegistrationBean fr = new FilterRegistrationBean();
        //将拦截器赋值
        fr.setFilter(myFilter);
        fr.setFilter(encodingFilter);
        //将拦截方式赋值
        fr.addUrlPatterns("/*");
        //返回
        return fr;
    }

}

