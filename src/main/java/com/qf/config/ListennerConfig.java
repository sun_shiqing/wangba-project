package com.qf.config;

import com.qf.listener.Mylistener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description:
 * @author: ssq
 * @time: 2020/8/5 21:01
 */
@Configuration
public class ListennerConfig {

    @Autowired
    private Mylistener mylistener;

    @Bean
    public ServletListenerRegistrationBean getMylistener(){
        ServletListenerRegistrationBean sr=new ServletListenerRegistrationBean();
        sr.setListener(mylistener);
        return sr;
    }

}

