package com.qf.filter;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

/**
 * @description:
 * @author: ssq
 * @time: 2020/8/5 20:48
 */
@Component
public class MyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("监听器发挥作用了");
        //放行
        chain.doFilter(request, response);
    }

}

