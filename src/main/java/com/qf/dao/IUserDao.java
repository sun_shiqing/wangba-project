package com.qf.dao;


import com.qf.entity.DTO.UserDTO;
import com.qf.entity.User;

public interface IUserDao {
    //检查当前在注册的手机号码是否已经存在
    User checkPhone(String uphone);

  //添加
    int addUser(User user);

    UserDTO findUser(Integer uId);

    void insertUser(Integer uId);
}
