package com.qf.dao;

import com.qf.entity.DTO.RoomDTO;
import com.qf.entity.Party;
import com.qf.entity.VoiceRoom;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RoomDao {
   //查询房间信息
   List<VoiceRoom>  getRoomList(RoomDTO roomDTO);

   List<Party> selectParty(Party party);
}
