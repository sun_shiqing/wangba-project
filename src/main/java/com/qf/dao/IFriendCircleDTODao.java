package com.qf.dao;

import com.qf.entity.DTO.FriendCircleDTO;

import java.util.List;

public interface IFriendCircleDTODao {

    /**
     * 根据用户的uId获取好友的玩吧uPlayerId
     *
     * @param uId
     * @return
     */
    List<Integer> getUserByUid(Integer uId);

    /**
     * 好友的uPlayerId获取所有的朋友圈集合数据
     *
     * @param list
     * @return
     */
    List<FriendCircleDTO> getFriendCircleDTOList(List<Integer> list);


}
