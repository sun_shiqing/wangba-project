package com.qf.dao;

import com.qf.entity.Message;
import com.qf.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMessageDao {
    /**
     * 根据用户id查找附近好友
     * @param uId
     * @return
     */
    List<User> findNearPeople(Integer uId);

    /**
     * 根据用户id查找消息列表
     * @param uId
     * @return
     */
    List<Message> findMsgList(Integer uId);
}
