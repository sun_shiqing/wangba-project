package com.qf.service.impl;

import com.qf.dao.IMessageDao;
import com.qf.entity.Message;
import com.qf.entity.User;
import com.qf.service.IMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsgServiceImpl implements IMsgService {
    @Autowired
    private IMessageDao messageDao;
    @Override
    public List<User> findNearPeople(Integer uId) {
        return messageDao.findNearPeople(uId);
    }

    @Override
    public List<Message> findMsgList(Integer uId) {
        return messageDao.findMsgList(uId);
    }
}
