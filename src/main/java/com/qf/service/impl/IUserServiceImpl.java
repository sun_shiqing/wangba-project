package com.qf.service.impl;


import com.qf.dao.IUserDao;
import com.qf.entity.DTO.UserDTO;
import com.qf.entity.User;
import com.qf.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IUserServiceImpl implements IUserService {


    @Autowired
    private IUserDao iUserDao;

    @Override
    public User checkPhone(String uPhone) {
        return iUserDao.checkPhone(uPhone);
    }

    @Override
    public int addUser(User user) {
        user.setuDistance(100);
        //随机生成uPlayerId
//        double v=(Math.random() * 9 + 1) * 10000;
//        int uPlayerId = new Double(v).intValue();
        user.setuPlayerId(1001);

        return iUserDao.addUser(user);
    }

    @Override
    public UserDTO findUser(Integer uId) {
        iUserDao.insertUser(uId);
        UserDTO userDTO = iUserDao.findUser(uId);
        return userDTO;
    }


}
