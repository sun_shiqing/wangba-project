package com.qf.service.impl;

import com.qf.dao.IFriendCircleDTODao;
import com.qf.entity.DTO.FriendCircleDTO;
import com.qf.service.IFriendCircleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendCircleServiceImpl implements IFriendCircleService {

    @Autowired
    private IFriendCircleDTODao dtoDao;

    /**
     * 根据用户id获取所有的朋友圈集合数据
     *
     * @param uId
     * @return
     */
    @Override
    public List<FriendCircleDTO> getFriendCircleDTOList(Integer uId) {
        //调用dao层方法获取所有的好友玩吧id集合
        List<Integer> list = dtoDao.getUserByUid(uId);
        //调用dao层获取朋友圈数据
        List<FriendCircleDTO> circleDTOS = dtoDao.getFriendCircleDTOList(list);
        //返回结果
        return circleDTOS;
    }
}

