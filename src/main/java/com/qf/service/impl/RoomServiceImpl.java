package com.qf.service.impl;

import com.qf.dao.RoomDao;
import com.qf.entity.DTO.RoomDTO;
import com.qf.entity.Party;
import com.qf.entity.VoiceRoom;
import com.qf.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomDao roomDao;

    @Override
    public List<VoiceRoom> selectAll(RoomDTO roomDTO) {
        return roomDao.getRoomList(roomDTO);
    }

    @Override
    public List<Party> selectParty(Party party) {
        return roomDao.selectParty(party);
    }

}
