package com.qf.service;

import com.qf.entity.Message;
import com.qf.entity.User;

import java.util.List;

public interface IMsgService {
    List<User> findNearPeople(Integer uId);
    List<Message> findMsgList(Integer uId);
}
