package com.qf.service;
import com.qf.entity.DTO.RoomDTO;
import com.qf.entity.Party;
import com.qf.entity.VoiceRoom;

import java.util.List;

public interface RoomService {
    List<VoiceRoom> selectAll(RoomDTO roomDTO);

    List<Party> selectParty(Party party);
}
