package com.qf.service;

import com.qf.entity.DTO.UserDTO;
import com.qf.entity.User;

public interface IUserService {

    User checkPhone(String uPhone);

    int  addUser(User user);

    UserDTO findUser(Integer uId);
}
