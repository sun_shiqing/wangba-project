package com.qf.service;

import com.qf.entity.DTO.FriendCircleDTO;

import java.util.List;

public interface IFriendCircleService {

    /**
     * 根据用户id获取所有的朋友圈集合数据
     * @param uId
     * @return
     */
    List<FriendCircleDTO> getFriendCircleDTOList(Integer uId);


}
