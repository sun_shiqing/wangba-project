package com.qf.entity.DTO;

import java.util.Date;

public class FriendCircleDTO {
    //用户id
    private Integer uId;
    //用户名
    private String uName;
    //用户头像
    private String uHeadPicture;
    //用户性别
    private Integer uGender;
    //用户玩吧id
    private Integer uPlayerId;
    //朋友圈id
    private Integer fId;
    //朋友圈信息
    private String fMsg;
    //朋友圈图片
    private String fPicture;
    //朋友圈点赞数
    private Integer fStarNum;
    //朋友圈评论数
    private Integer fCommentNum;
    //朋友圈转发数
    private Integer fForkNum;
    //朋友圈鲜花数
    private Integer fFlowerNum;
    //朋友圈发布的时间
    private Date fPubTime;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuHeadPicture() {
        return uHeadPicture;
    }

    public void setuHeadPicture(String uHeadPicture) {
        this.uHeadPicture = uHeadPicture;
    }

    public Integer getuGender() {
        return uGender;
    }

    public void setuGender(Integer uGender) {
        this.uGender = uGender;
    }

    public Integer getuPlayerId() {
        return uPlayerId;
    }

    public void setuPlayerId(Integer uPlayerId) {
        this.uPlayerId = uPlayerId;
    }

    public Integer getfId() {
        return fId;
    }

    public void setfId(Integer fId) {
        this.fId = fId;
    }

    public String getfMsg() {
        return fMsg;
    }

    public void setfMsg(String fMsg) {
        this.fMsg = fMsg;
    }

    public String getfPicture() {
        return fPicture;
    }

    public void setfPicture(String fPicture) {
        this.fPicture = fPicture;
    }

    public Integer getfStarNum() {
        return fStarNum;
    }

    public void setfStarNum(Integer fStarNum) {
        this.fStarNum = fStarNum;
    }

    public Integer getfCommentNum() {
        return fCommentNum;
    }

    public void setfCommentNum(Integer fCommentNum) {
        this.fCommentNum = fCommentNum;
    }

    public Integer getfForkNum() {
        return fForkNum;
    }

    public void setfForkNum(Integer fForkNum) {
        this.fForkNum = fForkNum;
    }

    public Integer getfFlowerNum() {
        return fFlowerNum;
    }

    public void setfFlowerNum(Integer fFlowerNum) {
        this.fFlowerNum = fFlowerNum;
    }

    public Date getfPubTime() {
        return fPubTime;
    }

    public void setfPubTime(Date fPubTime) {
        this.fPubTime = fPubTime;
    }
}

