package com.qf.entity.DTO;

/**
 * 房间模块查询的DTO
 */
public class RoomDTO {
    private Integer vId;
    private String vTitle;

    public RoomDTO() {
    }

    public RoomDTO(Integer vId, String vTitle) {
        this.vId = vId;
        this.vTitle = vTitle;
    }

    public Integer getvId() {
        return vId;
    }

    public void setvId(Integer vId) {
        this.vId = vId;
    }

    public String getvTitle() {
        return vTitle;
    }

    public void setvTitle(String vTitle) {
        this.vTitle = vTitle;
    }
}
