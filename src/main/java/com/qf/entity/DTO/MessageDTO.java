package com.qf.entity.DTO;

import com.qf.entity.Message;
import com.qf.entity.User;

import java.util.List;

/**
 * 消息页的DTO
 */
public class MessageDTO {
    private List<User> userList;
    private List<Message> msgList;

    public MessageDTO() {
    }

    public MessageDTO(List<User> userList, List<Message> msgList) {
        this.userList = userList;
        this.msgList = msgList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Message> getMsgList() {
        return msgList;
    }

    public void setMsgList(List<Message> msgList) {
        this.msgList = msgList;
    }
}
