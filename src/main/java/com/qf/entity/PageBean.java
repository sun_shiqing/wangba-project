package com.qf.entity;

/**
 * 分页实体
 */
public class PageBean {

    private Integer curPage = 1;
    private Integer pageSize = 3;

    public Integer getCurPage() {
        return curPage;
    }

    public void setCurPage(Integer curPage) {
        this.curPage = curPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

