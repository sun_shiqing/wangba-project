package com.qf.entity;

public class UsersParty {
        //用户id
        private Integer uId;
        //派对id
        private Integer pId;
        //语音房id
        private Integer vId;

        public Integer getuId() {
            return uId;
        }

        public void setuId(Integer uId) {
            this.uId = uId;
        }

        public Integer getpId() {
            return pId;
        }

        public void setpId(Integer pId) {
            this.pId = pId;
        }

        public Integer getvId() {
            return vId;
        }

        public void setvId(Integer vId) {
            this.vId = vId;
        }
}
