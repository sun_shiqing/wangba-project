package com.qf.entity;
    /**
     * 语音房实体
     */
    public class VoiceRoom {
        //语音房id
        private Integer vId;

        //语音房类型
        private String vType;

        //语音房图片
        private String vImage;

        //语音房标题--名字
        private String vTitle;

        //用户id
        private Integer uId;

        //语音房状态0关闭，1打开
        private Integer vStatus;

        //语音房信息
        private String vInfo;

        //存储用户信息（关系属性）
        private User user;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user=user;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_id
         *
         * @return the value of voiceroom.v_id
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public Integer getvId() {
            return vId;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_id
         *
         * @param vId the value for voiceroom.v_id
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvId(Integer vId) {
            this.vId = vId;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_type
         *
         * @return the value of voiceroom.v_type
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public String getvType() {
            return vType;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_type
         *
         * @param vType the value for voiceroom.v_type
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvType(String vType) {
            this.vType = vType == null ? null : vType.trim();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_image
         *
         * @return the value of voiceroom.v_image
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public String getvImage() {
            return vImage;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_image
         *
         * @param vImage the value for voiceroom.v_image
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvImage(String vImage) {
            this.vImage = vImage == null ? null : vImage.trim();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_title
         *
         * @return the value of voiceroom.v_title
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public String getvTitle() {
            return vTitle;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_title
         *
         * @param vTitle the value for voiceroom.v_title
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvTitle(String vTitle) {
            this.vTitle = vTitle == null ? null : vTitle.trim();
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.u_id
         *
         * @return the value of voiceroom.u_id
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public Integer getuId() {
            return uId;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.u_id
         *
         * @param uId the value for voiceroom.u_id
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setuId(Integer uId) {
            this.uId = uId;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_status
         *
         * @return the value of voiceroom.v_status
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public Integer getvStatus() {
            return vStatus;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_status
         *
         * @param vStatus the value for voiceroom.v_status
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvStatus(Integer vStatus) {
            this.vStatus = vStatus;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method returns the value of the database column voiceroom.v_info
         *
         * @return the value of voiceroom.v_info
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public String getvInfo() {
            return vInfo;
        }

        /**
         * This method was generated by MyBatis Generator.
         * This method sets the value of the database column voiceroom.v_info
         *
         * @param vInfo the value for voiceroom.v_info
         *
         * @mbggenerated Sat Aug 08 11:13:10 CST 2020
         */
        public void setvInfo(String vInfo) {
            this.vInfo = vInfo == null ? null : vInfo.trim();
        }
    }
