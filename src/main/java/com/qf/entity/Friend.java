package com.qf.entity;

/**
 * 用户和玩吧id的中间表
 */
public class Friend {
    //用户id
    private Integer uId;

    //玩吧id
    private Integer uPlayerId;

    //好友状态
    private Integer uStatus;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getuPlayerId() {
        return uPlayerId;
    }

    public void setuPlayerId(Integer uPlayerId) {
        this.uPlayerId = uPlayerId;
    }

    public Integer getuStatus() {
        return uStatus;
    }

    public void setuStatus(Integer uStatus) {
        this.uStatus = uStatus;
    }
}