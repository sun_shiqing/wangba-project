package com.qf.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;

/**
 * 用户实体
 */
public class User {
    //用户id
    private Integer uId;
    //用户名
    private String uName;
    //用户头像
    private String uHeadPicture;
    //用户性别
    private Integer uGender;

    //用户生日
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date uBirthday;
    //用户手机号
    private String uPhone;
    //用户玩吧id
    private Integer uPlayerId;
    //用户类型0普通,1为vip
    private Integer uType;
    //用户距离
    private Integer uDistance;

    public Integer getuGender() {
        return uGender;
    }


    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName == null ? null : uName.trim();
    }

    public String getuHeadPicture() {
        return uHeadPicture;
    }

    public void setuHeadPicture(String uHeadPicture) {
        this.uHeadPicture = uHeadPicture == null ? null : uHeadPicture.trim();
    }

    public void setuGender(Integer uGender) {
        this.uGender = uGender;
    }

    public Date getuBirthday() {
        return uBirthday;
    }

    public void setuBirthday(Date uBirthday) {
        this.uBirthday = uBirthday;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone == null ? null : uPhone.trim();
    }

    public Integer getuPlayerId() {
        return uPlayerId;
    }

    public void setuPlayerId(Integer uPlayerId) {
        this.uPlayerId = uPlayerId;
    }

    public Integer getuType() {
        return uType;
    }

    public void setuType(Integer uType) {
        this.uType = uType;
    }

    public Integer getuDistance() {
        return uDistance;
    }

    public void setuDistance(Integer uDistance) {
        this.uDistance = uDistance;
    }
}